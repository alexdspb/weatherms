# Weather Microservice

## Installation:

You should have Docker and Docker Compose installed on the target instance.

Copy file .env.example to .env and update the following environment variables:
- **DB_PASSWORD** with your desired password.
- **APININJAS_API_KEY** with your API key from your account at https://api-ninjas.com/.

Run the following command to create all containers for the application:

`docker compose up -d`

To stop all containers run:

`docker stop`


## Usage

### Add location

Adds new location with `lat`, `lng` and `name` parameters provided in request body.

POST /api/locations  
Content-Type: application/json

Body example:  
`
{
"lat": 44.800842,
"lng": 20.407459,
"name": "Block 58"
}
`

---

### Create shapshot

Creates a snapshot of current weather for the given weather service and location. 
Should be run ***hourly*** by cron or other task scheduler, e.g. Google Scheduler.  

GET /api/weather/{source}/snapshot/{location}

**URL Parameters**:

{source} - Weather service, 1 for OpenMeteo, 2 for ApiNinjas;  

{location} - The id of location from the database;

---

### Get Average Values
Returns average weather values for the given weather service, location and period.

GET /api/weather/{source}/average/{location}?period={period}

**URL Parameters**:

{source} - Weather service, 1 for OpenMeteo, 2 for ApiNinjas;  

{location} - The id of location from the database;  

{period} - 'day', 'week' or 'month';


---

## Architecture

The suggested architecture diagram can be found below:

![](/architecture-diagram.jpg)
