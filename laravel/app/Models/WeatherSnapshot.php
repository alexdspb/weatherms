<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeatherSnapshot extends Model
{
    use HasFactory;

    public const SOURCE_OPENMETEO = 1;
    public const SOURCE_APININJAS = 2;

    protected $fillable = ['source', 'location_id', 'temperature', 'wind'];

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
