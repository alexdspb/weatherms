<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\WeatherSnapshot;
use App\Services\WeatherServiceFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class WeatherController extends Controller
{
    private const PERIOD_DAY = 'day';
    private const PERIOD_WEEK = 'week';
    private const PERIOD_MONTH = 'month';

    public function __construct (private Logger $logger)
    {}

    public function shapshot(int $source, Location $location)
    {
        if (!in_array($source, WeatherServiceFactory::ALLOWED_SOURCES)) {
            return response()->json([
                'error' => 'Source is not allowed.',
            ], Response::HTTP_BAD_REQUEST);
        }

        try {
            $service = WeatherServiceFactory::createWeatherService($source);
            $snapshot = $service->createSnapshot($location->lat, $location->lng, $source, $location);

            return response()->json(
                $snapshot->toArray(),
                Response::HTTP_CREATED
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return response()->json([
                'error' => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    public function average(Request $request, int $source, Location $location)
    {
        $period = $request->query->get('period', self::PERIOD_DAY);
        $cacheKey = "api:weather:$source:average:{$location->id}:$period";

        $cachedAverages = Cache::get($cacheKey);
        if ($cachedAverages) {
            return response()->json($cachedAverages);
        }

        $createdFrom = match ($period) {
            self::PERIOD_DAY => Carbon::now()->subDay(),
            self::PERIOD_WEEK => Carbon::now()->subWeek(),
            self::PERIOD_MONTH => Carbon::now()->subMonth(),
            default => Carbon::now()->subDay(),
        };
        $averages = WeatherSnapshot::where('created_at', '>=', $createdFrom)
            ->selectRaw('AVG(temperature) AS avg_temperature, AVG(wind) AS avg_wind')
            ->first();
        Cache::put($cacheKey, $averages->toArray(), $seconds = 10);

        return response()->json($averages->toArray());
    }
}
