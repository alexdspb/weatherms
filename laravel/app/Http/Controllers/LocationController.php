<?php

namespace App\Http\Controllers;

use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class LocationController extends Controller
{
    public function create(Request $request)
    {
        $params = $request->json()->all();
        $validator = Validator::make($request->json()->all(), [
            'name' => 'required|max:50',
            'lat' => 'required|numeric|min:-90|max:90',
            'lng' => 'required|numeric|min:-180|max:180',
        ]);
        if ($validator->errors()->isNotEmpty()) {
            return response()->json([
                'errors' => $validator->errors()->all(),
            ], Response::HTTP_BAD_REQUEST);
        }

        try {
            $location = Location::create([
                'name' => $params['name'],
                'lat' => $params['lat'],
                'lng' => $params['lng'],
            ]);
            return response()->json($location->toArray(), Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
