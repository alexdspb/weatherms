<?php

namespace App\Services;

use App\Models\Location;
use App\Models\WeatherSnapshot;
use Illuminate\Support\Facades\Http;

class OpenMeteoService implements WeatherServiceInterface
{
    private const API_URL = 'https://api.open-meteo.com/v1/forecast';

    public function __construct()
    {
    }

    public function createSnapshot(float $lat, float $lng, int $source, Location $location): WeatherSnapshot
    {
        $response = Http::get(self::API_URL, [
            'latitude' => $lat,
            'longitude' => $lng,
            'current_weather' => 1,
        ]);
        $responseData = $response->json();
        if (!$response->ok() || !$responseData) {
            throw new \Exception('Snapshot creation failed.');
        }

        return WeatherSnapshot::create([
            'source' => $source,
            'location_id' => $location->id,
            'temperature' => $responseData['current_weather']['temperature'],
            'wind' => $responseData['current_weather']['windspeed'],
        ]);
    }

}
