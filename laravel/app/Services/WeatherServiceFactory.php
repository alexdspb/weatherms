<?php

namespace App\Services;

use Illuminate\Log\Logger;
use App\Models\WeatherSnapshot;

class WeatherServiceFactory
{
    public const ALLOWED_SOURCES = [
        WeatherSnapshot::SOURCE_OPENMETEO,
        WeatherSnapshot::SOURCE_APININJAS,
    ];

    public static function createWeatherService(int $source) {
        return match ($source) {
            WeatherSnapshot::SOURCE_OPENMETEO => new OpenMeteoService(),
            WeatherSnapshot::SOURCE_APININJAS => new ApiNinjasService(),
            default => throw new \Exception('Source is not valid.'),
        };
    }
}
