<?php

namespace App\Services;

use App\Models\Location;
use App\Models\WeatherSnapshot;

interface WeatherServiceInterface {
    public function createSnapshot(float $lat, float $lng, int $source, Location $location): WeatherSnapshot;
}
