<?php

namespace App\Services;

use App\Models\Location;
use App\Models\WeatherSnapshot;
use Illuminate\Support\Facades\Http;

class ApiNinjasService implements WeatherServiceInterface
{
    private const API_URL = 'https://api.api-ninjas.com/v1/weather';

    public function __construct()
    {
    }

    public function createSnapshot(float $lat, float $lng, int $source, Location $location): WeatherSnapshot
    {
        $response = Http::withHeaders([
            'X-Api-Key' => getenv('APININJAS_API_KEY')
        ])->get(self::API_URL, [
            'lat' => $lat,
            'lon' => $lng,
        ]);
        $responseData = $response->json();
        if (!$response->ok() || !$responseData) {
            throw new \Exception('Snapshot creation failed.');
        }

        return WeatherSnapshot::create([
            'source' => $source,
            'location_id' => $location->id,
            'temperature' => $responseData['temp'],
            'wind' => $responseData['wind_speed'],
        ]);
    }

}
