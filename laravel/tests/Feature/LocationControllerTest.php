<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LocationControllerTest extends TestCase
{
    public function test_location_creation_returns_an_id()
    {
        $payload = [
            'lat' => 44.800842,
            'lng' => 20.407459,
            'name' => 'Block 58',
        ];

        $response = $this->json('POST', '/api/locations', $payload);

        $response
            ->assertStatus(201)
            ->assertJsonIsObject()
            ->assertJsonStructure(['id'])
        ;
    }
}
